package com.calc.exception;

public class NegativeNumberException extends RuntimeException
{
	public NegativeNumberException()
	{
		super();
	}
	
	public NegativeNumberException(String msg)
	{
		super(msg);
	}

}
