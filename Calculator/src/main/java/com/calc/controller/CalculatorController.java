package com.calc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.calc.exception.NegativeNumberException;
import com.calc.model.Input;
import com.calc.service.CalculatorService;

@RestController
@RequestMapping(value = "/api/calc")
public class CalculatorController 
{
	@Autowired
	private CalculatorService service;
	
	@PostMapping(value = "/add")
	public ResponseEntity<Object> add(@RequestBody Input input)
	{
		ResponseEntity<Object> response = null;
		if (input.getFirstNumber() < 1 || input.getSecondNumber() < 1) 
		{
			throw new NegativeNumberException("Please Enter Positive Numbers");
		}
		Double result = service.add(input);
		response = new ResponseEntity<>(result, HttpStatus.OK);

		return response;
	}
	
	@PostMapping(value = "/sub")
	public ResponseEntity<Object> diff(@RequestBody Input input)
	{
		
		ResponseEntity<Object> response = null;
		if (input.getFirstNumber() < 1 || input.getSecondNumber() < 1) 
		{
			throw new NegativeNumberException("Please Enter Positive Numbers");
		}
		Double result = service.diff(input);
		response = new ResponseEntity<>(result, HttpStatus.OK);

		return response;
	}
	
	@PostMapping(value = "/product")
	public ResponseEntity<Object> product(@RequestBody Input input)
	{
		ResponseEntity<Object> response = null;
		if (input.getFirstNumber() < 1 || input.getSecondNumber() < 1)
		{
			throw new NegativeNumberException("Please Enter Positive Numbers");
		}
		Double result = service.product(input);
		response = new ResponseEntity<>(result, HttpStatus.OK);

		return response;
	}
	
	@PostMapping(value = "/division")
	public ResponseEntity<Object> division(@RequestBody Input input)
	{
		ResponseEntity<Object> response = null;
		if (input.getFirstNumber() < 1 || input.getSecondNumber() < 1) 
		{
			throw new NegativeNumberException("Please Enter Positive Numbers");
		}
		Double result = service.division(input);
		response = new ResponseEntity<>(result, HttpStatus.OK);

		return response;
	}
	
	@GetMapping(value = "/check")
	public String serverCheck()
	{
		return "Server is UP !!!!!!";
	}
}
