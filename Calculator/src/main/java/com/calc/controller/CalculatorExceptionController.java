package com.calc.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.calc.exception.NegativeNumberException;

@ControllerAdvice
public class CalculatorExceptionController 
{
	@ExceptionHandler(value = NegativeNumberException.class)
	public ResponseEntity<Object> negativeNumberException(NegativeNumberException exception) 
	{
		return new ResponseEntity<>("Invalid Input , Please enter Valid Number . Exception : "+exception.getMessage(), HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(value = NullPointerException.class)
	public ResponseEntity<Object> nullPointerException(NullPointerException exception) 
	{
		return new ResponseEntity<>("NullPointerException . Exception : "+exception.getMessage(), HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(value = Exception.class)
	public ResponseEntity<Object> genericException(Exception exception) 
	{
		return new ResponseEntity<>("Exception : "+exception.getMessage(), HttpStatus.NOT_FOUND);
	}
}