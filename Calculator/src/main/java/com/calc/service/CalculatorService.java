package com.calc.service;

import org.springframework.stereotype.Service;

import com.calc.model.Input;

@Service
public class CalculatorService 
{
	public Double add(Input input)
	{
		return input.getFirstNumber() + input.getSecondNumber();
	}
	
	public Double diff(Input input)
	{
		return input.getFirstNumber() - input.getSecondNumber();
	}
	
	public Double product(Input input)
	{
		return input.getFirstNumber() * input.getSecondNumber();
	}
	
	public Double division(Input input)
	{
		return input.getFirstNumber() / input.getSecondNumber();
	}
}
